output "host" {
  value       = scaleway_k8s_cluster.default.kubeconfig[0].host
  description = "The K8s host"
}

output "token" {
  value       = scaleway_k8s_cluster.default.kubeconfig[0].token
  description = "The K8S token"
}

output "certificate" {
  value       = scaleway_k8s_cluster.default.kubeconfig[0].cluster_ca_certificate
  description = "The K8S cluster CA certificate"
}
