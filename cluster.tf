resource "scaleway_k8s_cluster" "default" {
  name        = "${var.prefix}-cluster"
  description = "Scaleway Kubernetes cluster"

  project_id = var.project_id
  region     = var.region

  version = var.k8s_version
  cni     = var.k8s_cni
  tags    = var.k8s_tags

  delete_additional_resources = true
}
