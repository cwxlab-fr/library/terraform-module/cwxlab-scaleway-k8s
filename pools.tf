resource "scaleway_k8s_pool" "default" {
  cluster_id = scaleway_k8s_cluster.default.id

  name   = "${var.prefix}-default"
  zone   = var.zone
  region = var.region

  node_type = var.k8s_pool_default_node_type
  size      = var.k8s_pool_default_size
  tags      = var.k8s_tags

  timeouts {
    create = var.pool_create_timeout
  }
}

resource "scaleway_k8s_pool" "performance" {
  cluster_id = scaleway_k8s_cluster.default.id

  name   = "${var.prefix}-performance"
  zone   = var.zone
  region = var.region

  node_type = var.k8s_pool_performance_node_type
  size      = var.k8s_pool_performance_size
  tags      = var.k8s_tags

  timeouts {
    create = var.pool_create_timeout
  }
}
