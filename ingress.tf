resource "scaleway_lb_ip" "nginx_ip" {
  count = var.enable_ingress ? 1 : 0

  zone       = var.zone
  project_id = scaleway_k8s_cluster.default.project_id
}

resource "helm_release" "nginx_ingress" {
  depends_on = [
    scaleway_k8s_pool.default,
    scaleway_k8s_pool.performance,
  ]

  count = var.enable_ingress ? 1 : 0

  name             = "${var.prefix}-nginx"
  namespace        = "${var.prefix}-ingress"
  create_namespace = true

  repository = "https://kubernetes.github.io/ingress-nginx"
  chart      = "ingress-nginx"

  set {
    name  = "controller.service.loadBalancerIP"
    value = scaleway_lb_ip.nginx_ip[0].ip_address
  }

  # enable proxy protocol to get client ip addr instead of loadbalancer one
  set {
    name  = "controller.config.use-proxy-protocol"
    value = "true"
  }
  set {
    name  = "controller.service.annotations.service\\.beta\\.kubernetes\\.io/scw-loadbalancer-proxy-protocol-v2"
    value = "true"
  }

  # indicates in which zone to create the loadbalancer
  set {
    name  = "controller.service.annotations.service\\.beta\\.kubernetes\\.io/scw-loadbalancer-zone"
    value = scaleway_lb_ip.nginx_ip[0].zone
  }

  # enable to avoid node forwarding
  set {
    name  = "controller.service.externalTrafficPolicy"
    value = "Local"
  }

  # enable this annotation to use cert-manager
  #set {
  #  name  = "controller.service.annotations.service\\.beta\\.kubernetes\\.io/scw-loadbalancer-use-hostname"
  #  value = "true"
  #}
}
