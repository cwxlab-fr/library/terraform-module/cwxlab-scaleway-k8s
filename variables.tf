variable "project_id" {
  type        = string
  description = "The project ID that will be used as default value for project-scoped resources."
}

variable "region" {
  type        = string
  description = "The region that will be used as default value for all resources."
  default     = "fr-par"
}

variable "zone" {
  type        = string
  description = "The zone that will be used as default value for all resources."
  default     = "fr-par-1"
}
variable "prefix" {
  type        = string
  description = "A prefix to add to resources name"
  default     = "cwxlab"
}

variable "k8s_version" {
  type        = string
  description = "Kubernetes version"
  default     = "1.24.7"
}

variable "k8s_cni" {
  type        = string
  description = "Container Network Interface"
  default     = "cilium"
}

variable "k8s_pool_default_node_type" {
  type        = string
  description = "The commercial type of the pool instances"
  default     = "PLAY2_NANO"
}

variable "k8s_pool_default_size" {
  type        = number
  description = "The size of the pool"
  default     = 1
}

variable "k8s_pool_performance_node_type" {
  type        = string
  description = "The commercial type of the pool instances"
  default     = "PLAY2_MICRO"
}

variable "k8s_pool_performance_size" {
  type        = number
  description = "The size of the pool"
  default     = 1
}

variable "k8s_node_root_volume_size_in_gb" {
  type        = number
  description = "The size of the system volume of the nodes in gigabyte"
  default     = 50
}

variable "k8s_tags" {
  type        = list(string)
  description = "Tags list"
  default     = ["k8s", "dev"]
}

variable "enable_ingress" {
  type        = bool
  description = "Enable to Ingress Service"
  default     = true
}

variable "pool_create_timeout" {
  type        = string
  description = "Timeout for create operations."
  default     = "10m"
}
