terraform {
  required_providers {
    scaleway = {
      source  = "scaleway/scaleway"
      version = "2.11.1"
    }
  }
  required_version = ">= 0.13"
}
